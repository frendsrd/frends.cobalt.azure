﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NUnit.Framework;

namespace Frends.Cobalt.Azure.EndPoints.Tests
{
    [TestFixture]
    public class AzureBlobEndpointTests
    {
        private string _connectionString;
        private string _containerName;
        private CloudBlobContainer _container;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _connectionString = ConfigurationManager.AppSettings["AzureStorageConnectionString"];
            _containerName = ConfigurationManager.AppSettings["BlobContainerName"];

            _container = CloudStorageAccount.Parse(_connectionString)
                .CreateCloudBlobClient()
                .GetContainerReference(_containerName);

            _container.CreateIfNotExists();

            if (_container.ListBlobs().Any())
            {
                throw new Exception("The container needs to be empty");
            }

        }

        [TearDown]
        public void Cleanup()
        {
            // Just delete all the blobs from the container, the test fixture checked that the container 
            // didn't contain any blobs previously, so we shouldn't delete anything else than our test files
            var blobs = _container.ListBlobs(useFlatBlobListing: true);
            foreach (var blob in blobs.Where(b => b is CloudBlockBlob).Cast<CloudBlockBlob>())
            {
                blob.Delete();
            }

        }

        private BlobEndPoint CreateBlobEndPoint(string fileName = "testFile.txt", string folder = "folder")
        {
            var endPoint = new BlobEndPoint(new TransferEndPointConfig
            {
                CustomParameters = string.Format("{{\"ConnectionString\": \"{0}\"}}", _connectionString),
                Directory = string.Format("/{0}/{1}", _containerName, folder),
                FileName = fileName,
                EndPointType = "",
            }, new Dictionary<string, object>());

            endPoint.Open();

            return endPoint;
        }

        public void WriteTextToBlob(string path, string content)
        {
            var blob = _container.GetBlockBlobReference(path);
            blob.UploadText(content);
        }

        [TestCase("*.*", true)]
        [TestCase("*.txt", true)]
        [TestCase("*.bar", false)]
        public void ListFiles_ShouldListFilesInFolder(string fileFilter, bool shouldFindFile)
        {
            WriteTextToBlob("folder1/foo.txt", "shouldShowUp");
            WriteTextToBlob("folder1/foo2.txt", "shouldShowUpToo");
            WriteTextToBlob("folder2/bar.txt", "shouldNotShowUp");

            var target = CreateBlobEndPoint(fileFilter, "folder1");

            var files = target.ListFiles();

            if (!shouldFindFile)
            {
                Assert.That(files.Any(), Is.False);
            }
            else
            {
                Assert.That(files.Count(), Is.EqualTo(2));
                Assert.That(files.Any(f => f.Name.Equals("foo.txt")), Is.True);
                Assert.That(files.Any(f => f.Name.Equals("foo2.txt")), Is.True);
            }
        }

        [Test]
        public void FileExists_ShouldDetectExistingFile()
        {
            var target = CreateBlobEndPoint(folder: "files");

            var randomFileName = Path.GetRandomFileName();
            _container.GetBlockBlobReference(Path.Combine("files", randomFileName)).UploadText("");

            Assert.That(target.FileExists(randomFileName), Is.True, "The file should have been found.");
            Assert.That(target.FileExists(Path.GetRandomFileName()), Is.False, "The file should not have been found.");
        }

        [Test]
        public void PutAndDelete_ShouldPutAndDeleteBlob()
        {
            const string folderName = "folder1";
            var target = CreateBlobEndPoint("someFileName", folderName);

            WithTempFile(tempFile =>
            {
                const string fileContent = "someFileContent";
                File.WriteAllText(tempFile, fileContent);

                var blobName = Path.GetRandomFileName();

                target.Put(tempFile, blobName);

                var blob = _container.GetBlockBlobReference(folderName + "/" + blobName);
                Assert.That(blob.Exists(), Is.True, "The blob should have been found in the folder1 blob directory.");
                Assert.That(blob.DownloadText(), Is.EqualTo(fileContent));

                target.Delete(blobName);

                var deletedBlob = _container.GetBlockBlobReference(folderName + "/" + blobName);
                Assert.That(deletedBlob.Exists(), Is.False, "The blob should have been deleted.");
            });
        }

        [Test]
        public void Append_ShouldAppendBlobContent()
        {
            const string folderName = "folder1";
            var target = CreateBlobEndPoint("someFileName", folderName);

            WithTempFile(tempFile =>
            {
                const string fileContent = "someFileContent";
                File.WriteAllText(tempFile, fileContent);

                var blobName = Path.GetRandomFileName();

                target.Put(tempFile, blobName);
                var blob1 = _container.GetBlockBlobReference(folderName + "/" + blobName);
                blob1.FetchAttributes();
                target.Append(tempFile, blobName);

                var blob = _container.GetBlockBlobReference(folderName + "/" + blobName);
                blob.FetchAttributes();
                Assert.That(blob.Exists(), Is.True, "The blob should have been found in the folder1 blob directory.");
                Assert.That(blob.DownloadText(), Is.EqualTo(fileContent + fileContent));
            });
        }

        [TestCase("folder1")]
        [TestCase("folder2")]
        [TestCase("")]
        public void Move_ShouldMoveFile(string destinationDirectory)
        {
            const string folderName = "folder1";
            var target = CreateBlobEndPoint("someFileName", folderName);

            WithTempFile(tempFile =>
            {
                const string fileContent = "someFileContent";
                File.WriteAllText(tempFile, fileContent);

                var originalBlobName = Path.GetRandomFileName();

                target.Put(tempFile, originalBlobName);

                var newBlobName = Path.GetRandomFileName();
                target.Move(originalBlobName, Path.Combine(destinationDirectory, newBlobName));

                var originalBlob = _container.GetBlockBlobReference(Path.Combine(folderName , originalBlobName));
                var newBlob = _container.GetBlockBlobReference(Path.Combine(destinationDirectory , newBlobName));

                Assert.That(originalBlob.Exists(), Is.False, "The original blob should not exist since it was deleted.");
                Assert.That(newBlob.Exists(), Is.True, "The original blob should have been moved to the new blob.");
                Assert.That(newBlob.DownloadText(), Is.EqualTo(fileContent));
            });
        }

        [Test]
        public void Rename_ShouldRenameFileInSameFolder()
        {
            const string folderName = "folder1";
            var target = CreateBlobEndPoint("someFileName", folderName);

            WithTempFile(tempFile =>
            {
                const string fileContent = "someFileContent";
                File.WriteAllText(tempFile, fileContent);

                var originalBlobName = Path.GetRandomFileName();

                target.Put(tempFile, originalBlobName);

                var newBlobName = Path.GetRandomFileName();
                target.Rename(originalBlobName,  newBlobName);

                var originalBlob = _container.GetBlockBlobReference(Path.Combine(folderName, originalBlobName));
                var newBlob = _container.GetBlockBlobReference(Path.Combine(folderName, newBlobName));

                Assert.That(originalBlob.Exists(), Is.False, "The original blob should not exist since it was renamed.");
                Assert.That(newBlob.Exists(), Is.True, "The original blob should have been renamed to the new blob name.");
                Assert.That(newBlob.DownloadText(), Is.EqualTo(fileContent));
            });
        }

        private void WithTempFile(Action<string> action)
        {
            var tempFile = Path.GetTempFileName();
            try
            {
                action.Invoke(tempFile);
            }
            finally
            {
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                }
            }
        }
    }
}
