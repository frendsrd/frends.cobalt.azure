﻿using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Frends.Cobalt.Azure.EndPoints
{
    public class LeaseHandler
    {
        public Lease AcquireLease(CloudBlockBlob blob)
        {
            var leaseId = blob.AcquireLease(TimeSpan.FromSeconds(60), Guid.NewGuid().ToString());
            var lease = new Lease(blob, leaseId);

            return lease;
        }
    }

    public class Lease : IDisposable
    {
        private readonly CloudBlockBlob _blob;
        private Timer _timer;
        public string Id { get; private set; }


        public Lease(CloudBlockBlob blob, string leaseId)
        {
            _blob = blob;
            Id = leaseId;
            _timer = new Timer(RenewLease, null, 0, (int)TimeSpan.FromSeconds(30).TotalMilliseconds);
        }

        private void RenewLease(object state)
        {
            _blob.RenewLease(AccessCondition.GenerateLeaseCondition(Id));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_timer != null)
                {
                    _timer.Dispose();
                    _timer = null;
                }

                try
                {
                    _blob.ReleaseLease(AccessCondition.GenerateLeaseCondition(Id));
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(string.Format("Could not release lease for blob {0}, the lease has already expired or will expire soon. Error:\n{1}", _blob.Uri, ex));
                }
            }
        }
    }
}
