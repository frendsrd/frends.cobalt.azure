﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Newtonsoft.Json;

namespace Frends.Cobalt.Azure.EndPoints
{
    public class BlobEndPoint : EndPointBase
    {
        public class Settings
        {
            public string ConnectionString { get; set; }
            public string SasToken { get; set; }
            public string StorageAccountName { get; set; }
        }

        private readonly Settings _settings;
        private CloudBlobClient _client;
        private readonly string _blobContainer;
        private readonly string _virtualPath;
        private CloudBlobContainer _container;
        private readonly BlobRequestOptions _blobRequestOptions = new BlobRequestOptions { RetryPolicy = new ExponentialRetry() };
        private readonly LeaseHandler _leaseHandler;

        public BlobEndPoint(TransferEndPointConfig endPointConfig, IDictionary<string, object> customData)
            : base(endPointConfig, customData)
        {
            _settings = JsonConvert.DeserializeObject<Settings>(endPointConfig.CustomParameters);

            var directoryParts = (string.IsNullOrEmpty(Dir) ? string.Empty : Dir).Split(new[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);

            if (directoryParts.Length < 1)
            {
                throw new ArgumentException("Cobalt blob Endpoint requires the directory to be set");
            }

            _blobContainer = directoryParts.First();

            var directoryPartsWithoutContainer = directoryParts.Skip(1).ToList();

            _virtualPath = directoryPartsWithoutContainer.Any() ? (string.Join("/", directoryPartsWithoutContainer) + "/") : null; // skip the container name, and recreate the path with forward slashes, null if it's the root

            _leaseHandler = new LeaseHandler();
        }

        public override void Dispose()
        {
        }

        public override void Delete(string remoteFile)
        {
            var blob = GetBlobReference(GetRemoteFilePath(remoteFile));
            blob.DeleteIfExists(DeleteSnapshotsOption.IncludeSnapshots, options: _blobRequestOptions);
        }

        public override string Rename(string remoteFile, string toFile)
        {
            return Move(remoteFile, Path.Combine(_virtualPath, toFile));
        }

        public override string Move(string remoteFile, string toPath)
        {
            // only support moving blobs inside the same container
            var origin = GetBlobReference(GetRemoteFilePath(remoteFile));
            var destination = GetBlobReference(toPath);

            // this might take a while, but it really shouldn't since it should be a shadow copy inside the same container
            destination.StartCopyFromBlob(source: origin, options: _blobRequestOptions);

            WaitForCopyToFinish(destination);

            // delete the origin since we're "moving" the blob
            origin.Delete();


            return toPath;
        }

        public void WaitForCopyToFinish(CloudBlockBlob copyDestination)
        {
            if (copyDestination.CopyState.Status == CopyStatus.Success)
            {
                return; // The copy has already finished, no need to wait
            }

            const int maxRetryCount = 2;
            var retryCount = 0;
            var maxWait = DateTime.Now.AddMinutes(1); // 

            while (DateTime.Now < maxWait)
            {
                if (copyDestination.CopyState.Status == CopyStatus.Success)
                {
                    break; // We're done here
                }
                if (copyDestination.CopyState.Status == CopyStatus.Aborted ||
                    copyDestination.CopyState.Status == CopyStatus.Failed)
                {
                    // The copy failed for some reason, lets retry a few times
                    if (retryCount < maxRetryCount)
                    {
                        Trace.WriteLine(string.Format("Copying blob from '{0}' to '{1}' failed: '{2}' Retrying.", copyDestination.CopyState.Source.AbsoluteUri, copyDestination.Uri, copyDestination.CopyState.StatusDescription));
                        copyDestination.Delete(DeleteSnapshotsOption.IncludeSnapshots); // 
                        copyDestination.StartCopyFromBlob(copyDestination.CopyState.Source);
                        retryCount++;
                    }
                    else
                    {
                        throw new Exception(string.Format("Copying blob from '{0}' to '{1}' failed: {2}", copyDestination.CopyState.Source.AbsoluteUri, copyDestination.Uri, copyDestination.CopyState.StatusDescription));
                    }
                }
                else if (copyDestination.CopyState.Status == CopyStatus.Pending)
                {
                    // wait 5 seconds for the copy to finish
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                }
            }
        }


        public override void Open()
        {
            _client = CreateBlobClient(_settings);
            _container = _client.GetContainerReference(_blobContainer);

            if (!_client.Credentials.IsSAS)
            {
                // Sas credentials cannot create a continaer
                _container.CreateIfNotExists(_blobRequestOptions);
            }
        }

        private CloudBlobClient CreateBlobClient(Settings settings)
        {
            if (!string.IsNullOrEmpty(settings.ConnectionString))
            {
                return CloudStorageAccount.Parse(settings.ConnectionString).CreateCloudBlobClient();
            }
            else if (!string.IsNullOrEmpty(settings.SasToken))
            {
                return
                    new CloudBlobClient(
                        new Uri(string.Format("https://{0}.blob.core.windows.net/", settings.StorageAccountName)),
                        new StorageCredentials(settings.SasToken));
            }

            throw new Exception("Either provide the ConnectionString or SasToken and StorageAccountName in the configuration");
        }

        public override void Close()
        {
            // nothing to do here, we don't have an active connection
        }

        public override void Get(string remoteFilePath, string localFilePath)
        {
            var blob = GetBlobReference(remoteFilePath);

            using (var lease = _leaseHandler.AcquireLease(blob))
            {
                blob.DownloadToFile(localFilePath, FileMode.Create, AccessCondition.GenerateLeaseCondition(lease.Id), options: _blobRequestOptions);
            }
        }

        private string GetRemoteFilePath(string remoteFile)
        {
            // check if the remote file path is contains a folder or is just file
            if (remoteFile.Contains("/"))
            {
                return remoteFile; // the remoteFile already contains a path
            }

            // the remoteFile did not contain a path, use the virtual path as a location
            return Path.Combine(_virtualPath, remoteFile);
        }

        private void WriteBlob(string remoteFile, Action<string, CloudBlockBlob> writeAction)
        {
            var blobPath = GetRemoteFilePath(remoteFile);
            var blob = GetBlobReference(blobPath);
            if (!blob.Exists())
            {
                blob.UploadFromByteArray(new byte[] { }, 0, 0); // ensure the blob is created before acquiring a lease
            }

            using (var lease = _leaseHandler.AcquireLease(blob))
            {
                writeAction.Invoke(lease.Id, blob);
            }
        }

        public override void Put(string sourceFile, string remoteFile)
        {
            WriteBlob(remoteFile, (lease, blob) =>
            {
                using (var readStream = File.OpenRead(sourceFile))
                using (var writeStream = blob.OpenWrite(AccessCondition.GenerateLeaseCondition(lease)))
                {
                    readStream.CopyTo(writeStream);
                }
            });
        }

        public override void Append(string sourceFile, string remoteFile)
        {
            WriteBlob(remoteFile, (lease, blob) =>
            {
                var blockIds = new List<string>();

                var blockList = blob.DownloadBlockList(BlockListingFilter.Committed, options: _blobRequestOptions, accessCondition: AccessCondition.GenerateLeaseCondition(lease));
                blockIds.AddRange(blockList.Select(b => b.Name));

                var blockSize = blob.StreamWriteSizeInBytes;

                var blockIdPrefix = Guid.NewGuid().ToString().Replace("-", "") + "-";

                using (var file = File.OpenRead(sourceFile))
                {
                    var blockId = 0;

                    while (file.Position < file.Length)
                    {
                        var bufferSize = blockSize < file.Length - file.Position ? blockSize : file.Length - file.Position;
                        var buffer = new byte[bufferSize];
                        // read data to buffer
                        file.Read(buffer, 0, buffer.Length);

                        // save data to memory stream and put to storage
                        using (var stream = new MemoryStream(buffer))
                        {
                            stream.Position = 0;

                            // the format of the storage clients block ids is guid(without dashes)-(blockIndex) e.g. 12bb5a7d52de49498943740ae21e10b3-000000
                            var blockIdBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(blockIdPrefix + blockId.ToString(CultureInfo.InvariantCulture).PadLeft(6, '0')));
                            blob.PutBlock(blockIdBase64, stream, null, accessCondition: AccessCondition.GenerateLeaseCondition(lease));
                            blockIds.Add(blockIdBase64);
                            // increase block id
                            blockId++;
                        }
                    }
                }

                // reset the content md5, we don't know what the file previously contained 
                // so it cannot be calculated
                blob.FetchAttributes();
                blob.Properties.ContentMD5 = null;
                blob.SetProperties(accessCondition:AccessCondition.GenerateLeaseCondition(lease));

                blob.PutBlockList(blockIds, accessCondition: AccessCondition.GenerateLeaseCondition(lease));
            });
        }

        public override IList<FileItem> ListFiles()
        {
            var list = _container.ListBlobs(_virtualPath, false, BlobListingDetails.None,
                _blobRequestOptions);
            var filteredList = list
                .Where(b => b is CloudBlockBlob).Cast<CloudBlockBlob>()
                .Where(b => b.Properties.LeaseState != LeaseState.Leased && b.Properties.LeaseState != LeaseState.Breaking) // don't list blobs that have a lease
                .Where(b => Util.FileMatchesMask(Path.GetFileName(b.Name), FileName))
                .Select(b => new BlobFileItem()
                {
                    BlobUri = b.Uri,
                    Name = Path.GetFileName(b.Name),
                    Size = b.Properties.Length,
                    Modified = (b.Properties.LastModified ?? DateTimeOffset.MinValue).DateTime
                })
                .Cast<FileItem>()
                .ToList();

            return filteredList;
        }

        public override bool FileExists(string remoteFile)
        {
            var reference = GetBlobReference(Path.Combine(_virtualPath,remoteFile));

            return reference.Exists();
        }

        private CloudBlockBlob GetBlobReference(string remoteFilePath)
        {
            return _container.GetBlockBlobReference(remoteFilePath);
        }
    }

    public class BlobFileItem : FileItem
    {
        public Uri BlobUri { get; set; }
    }
}
